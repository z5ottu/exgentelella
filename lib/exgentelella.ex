defmodule Exgentelella do
  @moduledoc """
  Documentation for Exgentelella.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Exgentelella.hello
      :world

  """
  def hello do
    :world
  end
end
